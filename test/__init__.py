import os
import sys

PROJECT_PATH = os.getcwd()
SOURCE_PATH = os.path.join(PROJECT_PATH, "src")
sys.path.append(SOURCE_PATH)

from flask_testing import TestCase
from src.app import app
from src import configs


class BaseTestCase(TestCase):
    def create_app(self):
        # 必要。須回傳 Flask 實體。
        app.config.from_object(configs.TestingConfig)
        return app
