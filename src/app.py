from flask import Response, Flask, request, jsonify
import prometheus_client
from prometheus_client.core import CollectorRegistry
from prometheus_client import Summary, Counter, Histogram, Gauge
import time, os, socket
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
import configs as CONFIGS

app = Flask(__name__)
app.config.from_object(CONFIGS)

_INF = float("inf")
graphs = {}
graphs["c"] = Counter(
    "python_request_operations_total", "The total number of processed requests"
)
graphs["h"] = Histogram(
    "python_request_duration_seconds",
    "Histogram for the duration in seconds.",
    buckets=(1, 2, 5, 6, 10, _INF),
)

# Configure database connection using environment variable
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get(
    "DATABASE_URL", "postgresql://u:p@db:5432/db"
)
db = SQLAlchemy(app)


# Define the QueryHistory model
class QueryHistory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    client_ip = db.Column(db.String)
    domain = db.Column(db.String)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, client_ip, domain):
        self.client_ip = client_ip
        self.domain = domain

        with app.app_context():
            db.create_all()


@app.route("/")
def root():
    response = {
        "version": "0.1.0",
        "date": int(datetime.now().timestamp()),
        "kubernetes": False,
    }
    return jsonify(response)


# History endpoint
@app.route("/v1/history")
def history():
    queries = (
        QueryHistory.query.order_by(QueryHistory.created_at.desc()).limit(20).all()
    )
    response = {
        "history": [
            {
                "client_ip": query.client_ip,
                "domain": query.domain,
                "created_at": query.created_at.timestamp(),
            }
            for query in queries
        ]
    }
    return jsonify(response)


# Lookup endpoint
@app.route("/v1/tools/lookup", methods=["GET"])
def lookup_domain():
    start = time.time()
    graphs["c"].inc()

    domain = request.args.get("domain")

    # Perform domain lookup logic and get addresses
    addresses = ["127.0.0.1", "192.168.1.1"]

    query = {
        "addresses": addresses,
        "client_ip": request.remote_addr,
        "created_at": int(time.time()),
        "domain": domain,
    }

    # query history
    query_db = QueryHistory(
        client_ip=request.remote_addr, domain=domain
    )  # Adjust domain
    db.session.add(query_db)
    db.session.commit()

    time.sleep(0.600)
    end = time.time()
    graphs["h"].observe(end - start)
    return jsonify(query)


# Validate endpoint
@app.route("/v1/tools/validate", methods=["POST"])
def validate():
    data = request.get_json()
    ip = data.get("ip")

    # Validate if the input is a valid IPv4 address
    try:
        socket.inet_pton(socket.AF_INET, ip)
        is_valid = True
    except socket.error:
        is_valid = False

    response = {"status": is_valid}

    if is_valid:
        return jsonify(response), 200
    else:
        return jsonify(response), 400


@app.route("/health")
def health():
    return "Healthy"


@app.route("/metrics")
def requests_count():
    res = []
    for k, v in graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype="text/plain")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3000)
